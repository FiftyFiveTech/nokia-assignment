package interfaces;

import models.menu.Menu;

public interface IManufacturerMenuHandler {
    void addQuantity(Menu menu);

    void listQuantity(Menu menu);
}
