package models.menu;

import com.google.gson.annotations.SerializedName;

public enum MenuType {
  @SerializedName("LINK")
  LINK,
  @SerializedName("LEAF")
  LEAF,
  @SerializedName("BACK")
  BACK
}


