package handlers.menuHandlers;

import interfaces.IMenuHandler;
import models.menu.Menu;

public class MenuLinkHandler implements IMenuHandler {

    @Override
    public void handleMenu(Menu menu) {
        menuOptions.showMenus(menu);
    }

    @Override
    public void goBack(Menu menu) {
        menuOptions.goBack(menu);
    }
}
