package handlers.menuHandlers;

import database.repository.ManufacturerRepository;
import database.repository.PartManufacturerRepository;
import interfaces.IDataMenuHandler;
import interfaces.IMenuHandler;
import models.menu.Menu;

import java.lang.reflect.InvocationTargetException;
import services.ManufacturerService;
import services.PartService;
import usecases.DataUseCase;
import usecases.ManufacturerUseCase;

public class DataMenuHandler implements IMenuHandler, IDataMenuHandler {

  DataUseCase useCase = new DataUseCase();

  @Override
  public void handleMenu(Menu menu) {
    try {
      this.getClass().getDeclaredMethod(menu.getMethod(), Menu.class).invoke(this, menu);
    } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void goBack(Menu menu) {
    menuOptions.goBack(menu);
  }

  @Override
  public void addPart(Menu menu) {
    useCase.startAddPartFlow();
    goBack(menu);
  }

  @Override
  public void addManufacturer(Menu menu) {
    useCase.startAddManufacturerFlow();
    goBack(menu);
  }

  @Override
  public void removeManufacturer(Menu menu) {
    useCase.startRemoveManufacturerFlow();
    goBack(menu);
  }
}
