package handlers.menuHandlers;

import interfaces.ICompanyMenuHandler;
import interfaces.IMenuHandler;
import models.menu.Menu;

import java.lang.reflect.InvocationTargetException;
import services.CompanyService;
import usecases.CompanyUseCase;
import usecases.DataUseCase;

public class CompanyMenuHandler implements IMenuHandler, ICompanyMenuHandler {

  CompanyUseCase useCase = new CompanyUseCase();

  @Override
  public void handleMenu(Menu menu) {
    try {
      this.getClass().getDeclaredMethod(menu.getMethod(), Menu.class).invoke(this, menu);
    } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void goBack(Menu menu) {
    menuOptions.goBack(menu);
  }

  @Override
  public void addMoney(Menu menu) {
    useCase.startAddMoneyFlow();
    goBack(menu);
  }

  @Override
  public void buyParts(Menu menu) {
    useCase.startBuyPartsFlow();
    goBack(menu);
  }

  @Override
  public void listParts(Menu menu) {
    useCase.startListPartsFlow();
    goBack(menu);
  }
}
