# Nokia Task

This a demo command line Java application that aims at performing certain database transactions
revolving around a business case of a Company trying to buy Part from a host of Manufacturers.

The application provides a Menu system that can be navigated by the user and perform operations.


##Run the project
1. Setup jdk 17 in your system
2. Clone repository 
3. Rebuild project 
4. Run Main.java file
 
## Database Schema

We defined 5 entities -

1. Company
2. Part
3. Manufacturer
4. PartManufacturer
5. CompanyPart

## Technologies Used

1. Language Used - **Java**
2. Dependency Manager - **Maven** 
3. Database used - **H2**
4. ORM - **Hibernate**

## Architecture

### Menu System

For this project we have created a completely dynamic menu system. This consists of two parts:
1. A JSON file describing individual menus and the complete menu hierarchy.
2. A parser, that can parse this menu json and render a complete menus and also performs the logic or method that should 
be triggered when a menu item is selected.

A Menu item looks as follows -
```json
{
  "id": "menu-0.0",
  "disName": "Add part",
  "disNumber": 1,
  "type": "LEAF",
  "category": "DATA",
  "parentId": "menu-0",
  "children": [],
  "method": "addPart"
}
```
1. `id` - Indicating the id of the menu. This follows a certain nomenclature for numbering -> menu-{parent_id}.{child_id}
2. `disName` - The display name for the menu, this is visible in Menus
3. `disNumber` - The display number for the menu, this is used to evaluate the chosen option. This should be UNIQUE in menus
4. `type` - This defines the type of the menu. Possible types are - 
   * `LINK` - Menu opens a new Menu.
   * `LEAF` - Menu performs some action/operation or starts new terminal flow.
   * `BACK` - Menu that goes back to previous Menu.
5. `category` - The category of the menu, defined by the parent.
6. `parentId` - The id of the parent menu. 
7. `children` - A list of child menus. Defaults to `[]`.
8. `method` - The method that should be invoked when this menu is selected. Defaults to `""`.

**One can create new menus categories and options just by adding a new item or a set of items to `menus.json` and 
provide support for the handler in the code.**

###NOTE
**Due to lack of time ,I was unable to write Unit Tests. If given more time, I can write them as well**
